# O
- Three design patterns: Observer pattern, command pattern and strategy pattern. Today we have studied the definition, use case, code structure of these three design patterns, and studied these patterns in depth through several cases. Through learning, I gradually understand the difference between command mode and strategy mode.
- Refactor: Refactor is a change made to the internal structure of software without changing its observable behavior. It includes 4 parts: test protection, find code smell, refactor techniques and baby step. Today we mainly study find code smell.
- Code smell: In computer programming, a code smell is any characteristic in the source code of a program that possibly indicates a deeper problem. Mysterious name, duplicated code, long function is common in code smell.
# R
I am full of energy.
# I
Today I learn a new concept name code smell. I was aware of some bad code types before, but I didn't know the term code smell.
# D
I will try my best to learn all kinds of code smells, I believe that learning them can improve the quality of my code.