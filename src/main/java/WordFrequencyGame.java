import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String GAME_STRING_SEPARATOR = "\\s+";
    public static final String LINE_BREAK = "\n";

    public String getWordFrequencyString(String inputStr) {

        String[] inputWords = inputStr.split(GAME_STRING_SEPARATOR);

        if (inputWords.length == 1) {
            return inputStr + " 1";
        }

        try {

            return Arrays.stream(inputWords)
                    .collect(Collectors.groupingBy(String::valueOf, Collectors.counting()))
                    .entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .map(inputWord -> inputWord.getKey() + " " + inputWord.getValue())
                    .collect(Collectors.joining(LINE_BREAK));

        } catch (Exception e) {
            return "Calculate Error";
        }

    }


}
